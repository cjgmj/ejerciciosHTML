# Vídeos

## Video

```HTML
<video></video>
```

Introduce un vídeo en la página web.

### Atributos

#### Src

```HTML
src=""
```

Indica la ruta donde se encuentra el vídeo.

#### Controls

```HTML
controls
```

Muestra en pantalla los controles del vídeo.

#### Autoplay

```HTML
autoplay
```

Indica que el vídeo empieza a reproducirse al cargar la página. Los navegadores actuales bloquean el atributo autoplay cuando el contenido no está silenciado. Puede que este atributo en local funcione, pero en un dominio real es muy probable que falle.

#### Muted

```HTML
muted
```

Indica que el contenido estará silenciado al cargarse. De esta forma el `autoplay` no se bloquearía.

#### Loop

```HTML
loop
```

Indica que cuando acabe el vídeo volverá a reproducirse desde el principio.

#### Poster

```HTML
poster=""
```

Indica la imagen que hará de miniatura para el vídeo. Es decir, lo que se verá antes de que se reproduzca el vídeo.
