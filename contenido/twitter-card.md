# Twitter card

Funciona parecedio a `Open Graph protocol` pero es exclusivo para Twitter. Se puede validar la tarjeta creada [aquí](https://cards-dev.twitter.com/validator). Hay distintos tipos de tarjetas las cuales se pueden consultar [aquí](https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/abouts-cards).

## Estructura

```HTML
<meta name="twitter:card" content="">
<meta name="twitter:site" content="">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="">
```

Este es la estructura básica para crear un `Twitter card`, solo habría que añadir el contenido del atributo `content`. Se puede consultar todos los tags [aquí](https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/markup) y su equivalencia con `Open Graph protocol`.

[Volver al índice](../README.md)
