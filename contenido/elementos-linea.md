# Elementos de línea

## Em (Emphasis)

```HTML
<em></em>
```

Añade énfasis al texto. En caso de querer marcar las palabras o frases con importancia tendrán que estar contenidos por esta etiqueta.

## Strong

```HTML
<strong></strong>
```

Añade más énfasis al texto que la etiqueta `em`. En caso de querer marcar las palabras o frases con más importancia tendrán que estar contenidos por esta etiqueta.

## Small

```HTML
<small></small>
```

Le quita énfasis al texto. En caso de querer marcar palabras o frases con poca relevancia tendrán que estar contenidos por esta etiqueta.

## Br (Line break)

```HTML
<br>
```

Añade un salto de línea. También se puede encontrar la etiqueta como `<br />`, esto se usaba en documentos xhtml, aunque no es un error en la especificación de HTML5 aparece como `<br>`. Son usados en los textos en los que los saltos de línea sea relevantes. No se deben usar para dar separación entre elementos.

## Wbr (Word break opportunity)

```HTML
<wbr>
```

Añade un salto de línea en caso de ser necesario. También pueden encontrarse como `<wbr />` al igual que pasa con la etiqueta `<br>`. Los navegadores interpretan los guiones (`-`) como una etiqueta `<wbr>`.

## Time

```HTML
<time></time>
```

Se usa para representar un contenido de fecha/hora. No es necesario que contenga la fecha y la hora, también es válido para contener solo la fecha o solo la hora.

## I (Italic)

```HTML
<i></i>
```

Pone en cursiva el texto. Se suele hacer con CSS porque es más cómodo y no ensucia el código. Hay un caso en el que esta etiqueta sí es importante, y es cuando se trabaja con librerías de iconos.

## B (Bold)

```HTML
<b></b>
```

Pone en negrita el texto. Se suele hacer con CSS porque es más cómodo y no ensucia el código.

## U (Underline)

```HTML
<u></u>
```

Pone subrayado el texto. Se suele hacer con CSS porque es más cómodo y no ensucia el código.

## Sup (Superscript)

```HTML
<sup></sup>
```

Añade un superíndice.

## Sub (Subscript)

```HTML
<sub></sub>
```

Añade un subíndice.

## Span

```HTML
<span></span>
```

Es un contenedor de línea, equivalente a `div`. Se suele usar para encerrar palabras o textos pequeños y darles estilo a través de CSS o localizarlos desde JavaScript. Semánticamente no significa nada.

## Q (Quote)

```HTML
<q></q>
```

Sirve para poner citas en línea, es el equivalente a `blockquote`.

### Atributos

#### Cite

```HTML
cite=""
```

Incluye un enlace al documento original o fuente. Al introducirle este atributo no lo convierte en un enlace, simplemente añade información adicional para el navegador.

## Code

```HTML
<code></code>
```

Sirve para encerrar código que se quiera representar visualmente. Suele ir unido con la etiqueta `pre`. Es importante usar esta etiqueta cuando se vaya a representar código visualmente.

## Mark

```HTML
<mark></mark>
```

Sirve para dar un color amarillo al fondo del texto contenido en la etiqueta.

[Volver al índice](../README.md)
