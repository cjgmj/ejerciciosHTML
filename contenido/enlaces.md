# Enlaces

## A (Anchor)

```HTML
<a href=""></a>
```

Son elementos de línea. Su objetivo es conectar una página web con otra página web, con un recurso tanto interno como externo, o con otro sitio web.

### Atributos

#### Href

```HTML
href=""
```

Es un atributo obligatorio. Especifica la ruta del recurso o sitio web.

#### Target

```HTML
target=""
```

Configura la forma en la que se quiere visualizar el recurso o sitio solicitado. Define donde se abrirá el recurso solicitado. Por norma general siempre que se use rutas absolutas tendrá como valor `_blank`. En rutas relativas este atributos prácticamente no se usa. Este atributo por defecto toma el valor `_self`, si no se añade este atributo funcionará del mismo modo.

#### Download

```HTML
download
```

Es un atributo booleano. Sirve para descargar el recurso solicitado. El recurso debe estar en el mismo servidor. Si el recurso no está en el servidor, aunque el enlace tenga este atributo no se descargará sino que se abrirá la página indicada.

## Rutas absolutas

Tienen un protocolo, http o https, y son únicas en la red. Se suelen utilizar para rutas externas.

## Rutas relativas

Pueden ser relativas al punto donde se encuentre la página o relativas a la raíz del proyecto. No usan protocolo. Si el recurso se encuentra al mismo nivel (en la misma carpeta) pondremos únicamente el nombre del archivo. Si necesitamos salir de la carpeta actual usaremos `../` y se pone uno por cada nivel (carpeta) de la que queramos salir. Para crear una ruta relativa a la raíz del proyecto hay que empezar con `/`, si solo se deja `/` se redigirá a `index.html` que es la página que los servidores y todos los navegadores intentan cargar por defecto. Es importante, siempre que sea posible, para los enlaces internos usar rutas relativas a la raíz.

## Navegación por anclas

Se puede ir directamente a un elemento de la página web poniendo en el `href` el id del elemento, por ejemplo `href="#id"`. Es importante poner el `#` antes del id, en caso contrario no funcionará. Es posible redireccionar desde otras páginas añadiendo el id a la ruta de la página en la que se encuentra el mismo, `href="./ruta/de/pagina#id"`.

[Volver al índice](../README.md)
