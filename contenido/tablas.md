# Tablas

## Table

```HTML
<table></table>
```

Etiqueta que engloba la tabla.

## Th (Table header)

```HTML
<th></th>
```

Etiqueta que construye el encabezado de un grupo de celdas.

## Tr (Table row)

```HTML
<tr></tr>
```

Etiqueta que construye una fila.

## Td (Table data)

```HTML
<td></td>
```

Etiqueta que construye una celda. El número de celdas dentro de un `tr` establece el número de columnas de la tabla.

### Atributos

#### Rowspan

```HTML
rowspan=""
```

Sirve para que una celda ocupe más de una fila. El valor por defecto es `1`.

#### Colspan

```HTML
colspan=""
```

Sirve para que una celda ocupe más de una columna. El valor por defecto es `1`.

## Caption

```HTML
<caption></caption>
```

Esta etiqueta es opcional. Representa el título de la tabla y según la especificación se coloca justo después de la etiqueta `table`.

## Colgroup

```HTML
<colgroup></colgroup>
```

Sirve para seleccionar una columna. Contendrá tantas etiquetas `col` como columnas tenga la tabla.

### Col

```HTML
<col>
```

Equivale a una columna siguiendo el mismo orden que tienen en la tabla. Una etiqueta `col` puede agrupar más de una columna.

#### Atributos

##### Span

```HTML
span=""
```

Indica el tamaño del grupo de columnas. El valor por defecto es `1`.

## Thead

```HTML
<thead></thead>
```

Representa la cabecera de la tabla. Dentro tendrá una etiqueta `tr` pero en el caso de las celdas se establecerán con `th` en vez de con `td`.

## Tbody

```HTML
<tbody></tbody>
```

Contiene el cuerpo de la tabla. Es opcional mientras no se esté utilizando un `thead`. Si existe un `thead` el resto de bloques de la tabla deben estar dentro de un `tbody`.

## Tfoot

```HTML
<tfoot></tfoot>
```

Esta etiqueta es opcional. Representa el pie de la tabla. Se puede ver en tablas que tienen suma de cantidades o un total.

[Volver al índice](../README.md)
