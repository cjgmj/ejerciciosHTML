# Otros tipos de inputs

## Color

```HTML
<input type="color">
```

Se utiliza para especificar un color.

## File

```HTML
<input type="file">
```

Se utiliza para cargar archivos y enviarlos desde un formulario.

## Hidden

```HTML
<input type="hidden">
```

Se utiliza para ocultar un campo. Puede contener valor pero no se mostrará en la página. Se puede recuperar el valor desde JavaScript.

## Range

```HTML
<input type="range">
```

Se utiliza para establecer un rango.

### Atributos

#### Step

```HTML
step=""
```

Determina el tamaño de los saltos. Por defecto el valor es `1`. Si se necesita una precisión de dos decimales, por ejemplo, se tiene que asignar el valor `0.01`. Para no tener en cuenta la cantidad de decimales se debe asignar el valor `any`.

#### Min

```HTML
min=""
```

Determina el valor mínimo.

#### Max

```HTML
max=""
```

Determina el valor máximo.

[Volver a Tipos de input](./tipos-input.md)
