# Audios

## Audio

```HTML
<audio></audio>
```

Introduce un audio en la página web. Dentro de la etiqueta se puede hacer uso de la etiqueta `source` indicándole el atributo `type` para introducir diferentes formatos del audio, funciona igual que el `srcset` para las imágenes. De esta forma si el navegador no puede reproducir un formato intentaría reproducir el audio de otro `source`.

### Atributos

#### Src

```HTML
src=""
```

Indica la ruta donde se encuentra el audio.

#### Controls

```HTML
controls
```

Muestra en pantalla los controles del audio.

#### Autoplay

```HTML
autoplay
```

Indica que la canción empieza a reproducirse al cargar la página. Los navegadores actuales bloquean el atributo autoplay cuando el contenido no está silenciado. Puede que este atributo en local funcione, pero en un dominio real es muy probable que falle.

#### Muted

```HTML
muted
```

Indica que el contenido estará silenciado al cargarse. De esta forma el `autoplay` no se bloquearía.

#### Loop

```HTML
loop
```

Indica que cuando acabe el audio volverá a reproducirse desde el principio.
