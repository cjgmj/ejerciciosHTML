# Tipos de input

1. [Inputs de texto](./input-texto.md)
2. [Inputs de números](./input-numeros.md)
3. [Inputs de tipo botón](./input-boton.md)
4. [Inputs de fecha](./input-fecha.md)
5. [Otros tipos de inputs](./otros-inputs.md)
6. [Inputs de selección](./input-seleccion.md)

[Volver a Formularios](../formularios.md)
