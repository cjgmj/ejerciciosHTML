# Contenido opcional relacionado

## Figure

```HTML
<figure></figure>
```

Sirve para insertar contenido relacionado pero que sea opcional su presencia, es decir, si se quitara la etiqueta `figure` el artículo seguiría teniendo sentido. Se utiliza para romper el flujo del contenido con un contenido relacionado pero que no es necesario para la interpretación del artículo. Sirve para insertar imágenes, tablas, gráficos, fragmento de código...

## Figcaption

```HTML
<figcaption></figcaption>
```

Sirve para poner un texto descriptivo al contenido de la etiqueta `figure`. Este texto aparece en la web en relación de como esté insertado en el código. Si la etiqueta está antes de la imagen, en la web el texto aparecerá encima de la imagen. Lo normal es que el texto esté debajo del elemento. Funciona como el pié del elemento.
