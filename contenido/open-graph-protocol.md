# Open Graph protocol

[Open Graph protocol](https://ogp.me/) es un protocolo creado por Facebook. Principalmente sirve para poder elegir que se muestra cuando se comparte nuestro sitio web. Es la información que se muestra al compartir un sitio web, por ejemplo, en una publicación de Facebook. Si no se indican estos elementos, la plataforma en la que se está compartiendo será la que decide lo que se muestra. Si no encuentra ningún contenido solo pondrá el link.

## Metadatos básicos

En todas las etiquetas se puede hacer uso del atributo `content` para indicar el valor del mismo.

### Title

```HTML
<meta property="og:title">
```

Indica el título del objeto.

### Type

```HTML
<meta property="og:type">
```

Indica el tipo del objeto.

### Image

```HTML
<meta property="og:image">
```

La ruta de la imagen que debe representar al objeto.

### Url

```HTML
<meta property="og:url">
```

La ruta del objeto. Está será usada como ID del mismo en el `graph`.

[Volver al índice](../README.md)
