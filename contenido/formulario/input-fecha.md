# Inputs de fecha

## Date

```HTML
<input type="date">
```

Se utiliza para introducir una fecha.

## Datetime

```HTML
<input type="datetime">
```

Obsoleto. Actualmente se comporta como un `input` de tipo texto.

## Datetime-local

```HTML
<input type="datetime-local">
```

Se utiliza para introducir una fecha y hora. No funciona en Firefox ni en Internet Explorer. En caso de necesitar introducir fecha y hora se recomienda usar las etiquetas `date` y `time` para seleccionar fecha y hora.

## Month

```HTML
<input type="month">
```

Se utiliza para introducir un mes de un año.

## Time

```HTML
<input type="time">
```

Se utiliza para introducir una hora.

## Week

```HTML
<input type="week">
```

Se utiliza para introducir el número de semana de un año.

[Volver a Tipos de input](./tipos-input.md)
