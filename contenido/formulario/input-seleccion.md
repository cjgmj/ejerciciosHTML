# Inputs de selección

## Radio

```HTML
<input type="radio">
```

Permite seleccionar una única opción de una lista de opciones relacionadas.

## Checkbox

```HTML
<input type="checkbox">
```

Permite seleccionar varias opciones de una lista de opciones relacionadas.

## Atributos de la etiqueta radio y la etiqueta checkbox

### Name

```HTML
name=""
```

Indica el nombre del conjunto de las opciones relacionadas.

### Value

```HTML
value=""
```

Indica el nombre con el que se envía el valor en el formulario.

### Checked

```HTML
checked
```

Indica que el valor está marcado por defecto.

## Select

```HTML
<select></select>
```

Crea una lista de opciones donde se puede seleccionar una o varias opciones. Por defecto es de selección individual. Por defecto el valor que selecciona es el primero de la lista.

### Atributos

### Name

```HTML
name=""
```

Indica el nombre con el que se envía el valor en el formulario.

#### Multiple

```HTML
multiple
```

Indica que la lista será de selección múltiple permitiendo así seleccionar varias opciones.

### Option

```HTML
<option></option>
```

Cada opción de un `select` irá dentro de un `option`.

### Optgroup

```HTML
<optgroup></optgroup>
```

Si hay muchas opciones se pueden agrupar por categorías a través de `optgroup`.

#### Atributos

##### Label

```HTML
label=""
```

Indica el nombre de la categoría que agrupa las opciones.

## List

```HTML
<input type="list">
```

Son muy similares a los `select` con la diferencia de que incluyen un filtro para que se pueda buscar de una forma más sencilla. Sirve para crear una lista de elementos con buscador. En la lista se muestra tanto el `value` como el texto de cada `option` que contenga, mostrando primero el valor del `value`. Se puede omitir el texto y de esa forma solo mostrar el valor del `value`.

### Atributos

#### List

```HTML
list=""
```

El atributo list tiene que tener el mismo valor que el id del `datalist`.

## Datalist

```HTML
<datalist></datalist>
```

Listado de elementos que se mostrarán en el `list`. Cada opción irá dentro de un `option`.

[Volver a Tipos de input](./tipos-input.md)
