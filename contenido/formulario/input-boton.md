# Inputs de tipo botón

## Button

```HTML
<input type="button">
```

Sirve para crear un botón en el formulario para hacer algún tipo de operación mediante JavaScript.

## Reset

```HTML
<input type="reset">
```

Se utiliza para resetear el formulario.

## Submit

```HTML
<input type="submit">
```

Se utiliza para enviar el formulario.

[Volver a Tipos de input](./tipos-input.md)
