# Listas

## Ul (Unordered list)

```HTML
<ul></ul>
```

Se utilizan cuando el orden de los elementos no influye. Por ejemplo para una lista de la compra. También son utilizados para construir los menús de navegación.

## Ol (Ordered list)

```HTML
<ol></ol>
```

Se utilizan cuando el orden de los elementos es importante. Por ejemplo para un top 10.

## Atributos

### Type

```HTML
type=""
```

Indica los estilos de los items.

- Los valores disponibles para los `ul` son `disc`, `square` y `circle`. El valor por defecto es `disc`.
- Los valores disponibles para los `ol` son `1`, `A`, `a` ,`I`, `i`. El valor por defecto es `1`.

### Start

```HTML
start=""
```

Solo está disponible para los `ol`. Indica el primer valor de la ordenación de la lista, es decir, el valor desde el cual se empezará a enumerar la lista.

## Li (List item)

```HTML
<li></li>
```

Representa cada uno de los elementos de la lista, tanto en las listas ordenadas como las desordenadas.

## Listas anidadas

```HTML
<ol>
    <li>
        Texto
        <ol>
            <li>...</li>
        </ol>
    </li>
</ol>
```

Se pueden construir listas anidadas teniendo en uno de los `li` otro `ul/ol` según sea necesario. La lista que la contiene puede ser tanto ordenadas como desordenadas. Se pueden anidar tantas listas como sean necesarias.

## Dl (Definition list)

```HTML
<dl></dl>
```

Se utilizan para hacer una lista de definiciones. Por ejemplo para un diccionario. Cada elemento de una lista de definición lleva las etiquetas `dt` y `dd`.

### Dt (Definition term)

```HTML
<dt></dt>
```

El término que se va a definir.

### Dd (Definition description)

```HTML
<dd></dd>
```

La descripción del término.

[Volver al índice](../README.md)
