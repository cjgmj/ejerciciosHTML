# Imágenes

Los formatos de imágenes para web se pueden clasificar en dos tipos generales:

- Vectoriales
  - svg (recomendado siempre que se pueda), la ventaja que tienen es que no añaden nada de peso a la web.
- Mapa de bits
  - jpg
  - png 8 y 24 (si se necesita transparencias)
  - gif (si se necesita una imagen animada)
  - webp (el formato que menos pesa), este es el formato más recomendado siempre que se trate de una imagen que no sea vectorial. Es recomendable que todas las imágenes se conviertan a webp para subirlas a una web. Otra ventaja es que este formato admite tanto transparencias como animaciones.

## Img

```HTML
<img>
```

Introduce una imagen en la página web. Si se quiere cambiar el tamaño de la imagen, aunque existen los atributos `width` y `height`, lo recomendable es hacerlo desde CSS. En algunos casos muy concretos sí se tiene que usar estos atributos, por ejemplo cuando las imágenes son dinámicas y se cargan desde JavaScript.

### Atributos

#### Src

```HTML
src=""
```

Indica la ruta donde se encuentra la imagen.

#### Alt

```HTML
alt=""
```

Es un atributo obligatorio. Debe ser un texto descriptivo de la imagen.

#### Srcset

```HTML
srcset=""
```

Indica la ruta donde se encuentra la imagen añadiéndole el tamaño de pantalla para el cual debe cargarla. Las rutas se separan por comas (`,`). Para indicar el tamaño tras las rutas hay que añadir el DPR para el cual debería cargar, `DPRx`, por ejemplo para móvil sería `imagen/ruta 2x`. Las rutas que no tengan el DPR indicado son las cargadas por defecto. En caso de que el navegador no pueda cargar la imagen por el tipo de la misma se puede añadir una nueva ruta. Internet Explorer no soporta las imagenes de tipo `webp` ni tampoco el atributo `srcset`, para este caso habría que añadir también el atributo `src`, de este modo `src` serviría como una copia de seguridad, ya que en el caso de no poder cargar nada se cargaría la imagen que tiene el atributo.

## DPR (Device Pixel Ratio)

Es la relación que existe entre los píxeles reales que tiene el dispositivo y los píxeles que hay disponibles para "pintar" contenido (viewport).

`DRP = píxeles reales / píxeles disponibles`

Aunque las imágenes en un móvil (DPR ≈ 2) se vean más pequeñas que en una pantalla (DPR ≈ 1) se suelen usar la misma imagen para ambos dispositivos siendo esta muy pesada y mucho más grande de lo que se necesita para mostrarla en un móvil. Para solucionar este error se usa el atributo `srcset`, de la etiqueta `img`, o la etiqueta `picture`.

## Picture

```HTML
<picture></picture>
```

Aún es una etiqueta experimental aunque tiene soporte en todos los navegadores principales. Sirve para introducir una imagen en la página web. Dentro de la etiqueta hay que hacer uso de la etiqueta `source` con el atributo `srcset`, funciona igual que en la etiqueta `img`. Hay que añadir de forma obligatoria la etiqueta `img` con el `src` que servirá de copia de seguridad dentro de la etiqueta `picture`. En caso de que no esté la etiqueta `img` no se mostrará nada. Se puede indicar el ancho de la imagen dentro de `source` con el atributo `media`. Este atributo sirve, por ejemplo, para indicarle a partir de que tamaño cargar una imagen u otra.

Por norma general se suele usar `picture` porque ofrece más opciones, pero puede que haya un caso en el que `picture` no esté soportado por el navegador pero la etiqueta `img` sí sea soportada.
