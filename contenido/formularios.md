# Formularios

El comportamiento por defecto de un formulario es recargar la página tras enviar la información introducida.

## Form

```HTML
<form></form>
```

Es la etiqueta que engloba el formulario. El método por defecto es `GET`.

## Label

```HTML
<label></label>
```

Sirve para escribir el nombre del campo a rellenar.

## Input

```HTML
<input>
```

Sirve para crear un campo que permitirá al usuario introducir información.

### [Tipos de inputs](./formulario/tipos-input.md)

## Asociar input y label

### For

```HTML
for=""
```

Es un atributo que debe llevar la etiqueta `label` el cual se relaciona con su `input` correspondiente mediante el id del mismo, por ejemplo `for="id"`.

### Input contenido

```HTML
<label>
  <input>
</label>
```

Es posible asociar el `label` al `input` siendo este contenido por el `label`. Esta forma no es muy común ya que limita más a la hora de aplicar estilos CSS.

## Fieldset

```HTML
<fieldset></fieldset>
```

Se utiliza para agrupar elementos dentro de un formulario.

## Legend

```HTML
<legend></legend>
```

Representa una etiqueta para el contenido del `fieldset`.

## Meter

```HTML
<meter></meter>
```

Representa un valor dentro de un rango conocido.

### Atributos

#### Min

```HTML
min=""
```

Determina el valor mínimo.

#### Max

```HTML
max=""
```

Determina el valor máximo.

#### Value

```HTML
value=""
```

Determina el valor actual.

#### Low

```HTML
low=""
```

Lo que esté por debajo del valor indicado se pintará de amarillo. En caso contrario se pintará de verde.

#### High

```HTML
high=""
```

Lo que esté por encima del valor indicado se pintará de amarillo. En caso contrario se pintará de verde.

#### Optimum

```HTML
optimum=""
```

Indica el valor óptimo.

## Progess

```HTML
<progress></progress>
```

Representa el progreso de una tarea.

### Atributos

#### Max

```HTML
max=""
```

Determina el valor máximo.

#### Value

```HTML
value=""
```

Determina el valor actual.

## Textarea

```HTML
<textarea></textarea>
```

Se utiliza para introducir texto en un formulario. Permite saltos de línea.

### Atributos

### Name

```HTML
name=""
```

Es un atributo obligatorio. Indica el nombre con el que se envía el valor en el formulario.

#### Cols

```HTML
cols=""
```

Número de columnas del `textarea`.

#### Rows

```HTML
rows=""
```

Número de filas del `textarea`.

## Button

```HTML
<button></button>
```

Crea un botón que permite enviar el formulario.

## Atributos para formularios

### Placeholder

```HTML
placeholder=""
```

Texto descriptivo de lo que se espera que se introduzca en el campo.

### Required

```HTML
required
```

Hace que un campo sea obligatorio.

### Readonly

```HTML
readonly
```

Hace que un campo sea de solo lectura.

### Disabled

```HTML
disabled
```

Desactiva el campo. No se podrá escribir en el autofocus. Los campos marcados con `disabled` no son enviados en el formulario.

### Min

```HTML
min=""
```

Establece el mínimo de un campo numérico.

### Max

```HTML
max=""
```

Establece el máximo de un campo numérico.

### Minlength

```HTML
minlength=""
```

Establece el mínimo de caracteres de un campo de texto. Para que funcione sin haber escrito el campo debe ser obligatorio.

### Maxlength

```HTML
maxlength=""
```

Establece el máximo de caracteres de un campo de texto. Para que funcione sin haber escrito el campo debe ser obligatorio.

### Selected

```HTML
selected
```

Sirve para establecer una opción por defecto en un `select`.

### Autofocus

```HTML
autofocus
```

Sirve para poner el foco por defecto al cargar el formulario.

[Volver al índice](../README.md)
