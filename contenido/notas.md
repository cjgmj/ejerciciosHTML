# Notas

- Para carpetas de desarrollo se debe evitar el uso de mayúsculas y espacios porque suelen dar problemas cuando se trabaja en un entorno real de producción. Es recomendable el uso de guiones para separar palabras.
- Siempre es recomendable trabajar con un servidor de desarrollo. Si la página se abre como un archivo, va a haber muchas cosas que no van a funcionar o que darán conflictos a la hora de visualizar el sitio web.
- No es recomendable usar nombres en español durante el desarrollo, lo correcto es usar el inglés.
- Asegurar que el lenguaje indicado en el HTML, en el atributo `lang`, se corresponde con el lenguaje del contenido de la página, en caso contrario tendrá una penalización por parte de CEO y de posicionamiento.
- En ningún caso se debe usar el HTML para dar estilos, es decir, usar atributos o etiquetas de estilos, por ejemplo, la etiqueta `center` la cual está obsoleta, o el atributo `border` en la etiqueta `table`. Esto es muy mala práctica.
- No se debe omitir niveles de encabezados.
- No se debe usar `br` para dar separación entre elementos.
- No se debe usar `hr` para dibujar líneas en el navegador.
- Para enlaces internos siempre que sea posible se debe usar rutas relativas a la raíz.

[Volver al índice](../README.md)
