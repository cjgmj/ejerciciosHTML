# Etiquetas del head

## Title

```HTML
<title></title>
```

Indica el título de la web. Es el texto que aparece en la pestaña del navegador.

## Etiquetas meta

### Charset

```HTML
<meta charset="">
```

Indica la códificación del sitio web.

### Description

```HTML
<meta name="description" content="">
```

Indica un texto descriptivo de la web que se mostrará cuando se busca el sitio web.

### Author

```HTML
<meta name="author" content="">
```

Indica quien es el dueño o quien ha construido el sitio web.

### Atributos

La mayoría de metadatos tendrán los atributos `name` y `content`.

#### Name

```HTML
name=""
```

Indica que tipo de metadato se le está dando al navegador.

#### Content

```HTML
content=""
```

Indica el contenido del metadato.

## Favicon

```HTML
<link rel="icon">
```

Es la imagen que aparece al lado del nombre de la web en la pestaña del navegador. Con esta instrucción solo funciona en navegadores actualizados, para dar soporte a todos los navegadores y móviles se debe hacer como en el [ejempo](../ejercicios/meta/index.html).

### Atributos

#### Type

```HTML
type="image/*"
```

Indica el tipo de la imagen. Un ejemplo sería `image/png`.

#### Href

```HTML
href=""
```

Indica la ruta donde se encuentra la imagen.

[Volver al índice](../README.md)
