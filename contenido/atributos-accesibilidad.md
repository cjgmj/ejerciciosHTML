# Atributos de accesibilidad

## Tabindex

```HTML
tabindex=""
```

Indica la ruta que se usará para desplazarse por la página web al hacer uso del tabulador. Normalmente se usa en las zonas donde se quiere navegar a través del tabulador, por ejemplo en un formulario.

## Role

```HTML
role=""
```

Le da al navegador información sobre el tipo de elemento en el que está posicionado.

## Aria

```HTML
aria-*=""
```

Sirve para indicarle al navegador información adicional para aplicaciones o extensión que lean el sitio web. Un ejemplo sería `aria-label`.

[Volver al índice](../README.md)
