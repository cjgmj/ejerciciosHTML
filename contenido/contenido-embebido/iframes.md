# Iframes

## Iframe

```HTML
<iframe></iframe>
```

Inserta webs externas en otra web. Al cargar la web implica más tiempo de carga. No hay que abusar de los iframe porque ralentiza la carga del sitio web.

### Atributos

#### Width

```HTML
width=""
```

Es obligatorio. Indica el alto del iframe.

#### Height

```HTML
height=""
```

Es obligatorio. Indica el ancho del iframe.

#### Src

```HTML
src=""
```

Es obligatorio. Indica la ruta del iframe.
