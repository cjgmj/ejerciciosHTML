# ejerciciosHTML

## Índice

1. [Sintaxis HTML](./contenido/sintaxis-html.md)
2. [Elementos de bloque](./contenido/elementos-bloque.md)
3. [Elementos de línea](./contenido/elementos-linea.md)
4. [Atributos generales](./contenido/atributos-generales.md)
5. [Enlaces](./contenido/enlaces.md)
6. [Listas](./contenido/listas.md)
7. [Tablas](./contenido/tablas.md)
8. [Formulario](./contenido/formularios.md)
9. [Contenido embebido](./contenido/contenido-embebido.md)
10. [Etiquetas del head](./contenido/etiquetas-head.md)
11. [Atributos de accesibilidad](./contenido/atributos-accesibilidad.md)
12. [Open Graph protocol](./contenido/open-graph-protocol.md)
13. [Twitter card](./contenido/twitter-card.md)
14. [Notas](./contenido/notas.md)

---

[Código ASCII](https://ascii.cl/es/codigos-html.htm)

[Referencia de Elementos HTML](https://developer.mozilla.org/es/docs/Web/HTML/Element)

---

Apuntes creados a partir del [curso](https://www.youtube.com/playlist?list=PLROIqh_5RZeB92ME1GFyeqDVOa-gL0Ybd) de [@DorianDesings](https://github.com/DorianDesings)
