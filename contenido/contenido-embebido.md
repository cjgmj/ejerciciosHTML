# Contenido embebido

Es todo el contenido que se muestra en la web pero que no forma parte del código. Estos son los archivos que más peso (tamaño) añaden a un sitio web. Los tipos más conocidos son:

- [Imágenes](./contenido-embebido/imagenes.md)
- [Audios](./contenido-embebido/audios.md)
- [Vídeos](./contenido-embebido/videos.md)
- [Iframes](./contenido-embebido/iframes.md)
- [Contenido opcional relacionado](./contenido-embebido/contenido-opcional-relacionado.md)

[Volver al índice](../README.md)
