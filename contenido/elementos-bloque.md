# Elementos de bloque

Los elementos de bloque ocupan todo el ancho disponible aunque su contenido no lo haga, por lo que los elementos que se pongan a continuación saltarán a la siguiente línea.

## P (Paragraph)

```HTML
<p></p>
```

Son los apropiados para distribuir el texto en párrafos.

## Encabezados (Heading)

```HTML
<h1></h1>
<h2></h2>
<h3></h3>
<h4></h4>
<h5></h5>
<h6></h6>
```

Implementan seis niveles de encabezado del documento siendo `h1` el más importante y `h6` el menos importante. Se debe evitar omitir niveles de encabezados. No se deben usar niveles inferiores para reducir el tamaño de la fuente, para ello usar la propiedad CSS `font-size`.

## Etiquetas de sección de contenido

### Header

```HTML
<header></header>
```

Cabecera de la página. Suele contener el menú de navegación, el logotipo de la página y ese tipo de cosas.

### Main

```HTML
<main></main>
```

Contenido principal de la página. Contiene todo el groso de la web.

### Footer

```HTML
<footer></footer>
```

Pie de la página. Suele contener el copyright, enlaces a las páginas de contacto, términos y condiciones, privacidad y demás enlaces de información sobre la web.

## Section

```HTML
<section></section>
```

Contenedor genérico que agrupa contenido que está relacionado. Se usa cuando se crean bloques cuyo contenido es parte de un bloque total. Un section puede contener más etiquetas `section` dentro de él.

## Article

```HTML
<article></article>
```

Contenedor que representa contenido independiente, es decir, se puede leer ese fragmento en cualquier otro sitio y tendría sentido por sí mismo. Como son contenido independientes pueden tener su propio header y su propio footer. Deben contener un encabezado (`heading`).

## Aside

```HTML
<aside></aside>
```

Se utiliza para representar contenido indirectamente relacionado pero que no forma parte del contenido principal. Puede estar tanto fuera como dentro de la etiqueta `main`.

## Anidamiento section y article

La etiqueta `section` puede contener varias etiquetas `article`. Por ejemplo, si se tienen varios artículos que hablan sobre etiquetas HTML, deben ir dentro de un `section`, ya que es contenido relacionado entre sí, y los `article` serían contenido independiente porque se podría leer uno sin haber leído el resto y seguiría teniendo sentido.

El `article` es definido como un componente de la página con un contenido independiente, esto implica que esta etiqueta pueda tener un `header` y un `footer` propio.

También existe el caso en el que un `article` contenga varias secciones. El `article` independiente podría ser navegadores, y este contener dentro secciones como, por ejemplo, navegadores más utilizados en 2020.

## Address

```HTML
<address></address>
```

Se utiliza para aportar información de contacto para el `article` más cercano o para todo el `body`. Dentro de la etiqueta `address` sería correcto utilizar la etiqueta `br` en caso de ser necesario.

## Blockquote

```HTML
<blockquote></blockquote>
```

Se utiliza para marcar las citas a otros autores o documentos.

### Atributos

#### Cite

```HTML
cite=""
```

Incluye un enlace al documento original o fuente. Al introducirle este atributo no lo convierte en un enlace, simplemente añade información adicional para el navegador.

## Pre

```HTML
<pre></pre>
```

Se utiliza para tener texto preformateado que necesita ser representado igual que se escribió. Cuando se escribe código suele ir acompañado de la etiqueta `code`.

## Div

```HTML
<div></div>
```

Se usa como división del contenido del documento. Semánticamente no significa nada, es un contenedor genérico que se usa generalmente para dar estilos a través de CSS o para usar algo denominado "delegación de eventos" en JavaScript.

## Hr (Horizontal rule)

```HTML
<hr>
```

Se utiliza para indicarle al navegador que se va a realizar un cambio de tema. No se deben usar para dibujar líneas en el navegador.

[Volver al índice](../README.md)
