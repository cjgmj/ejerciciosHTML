# Sintaxis HTML

## Etiquetas

```HTML
<nombreEtiqueta>Contenido de la etiqueta</nombreEtiqueta>
```

`<nombreEtiqueta>` se denomina etiqueta de apertura y `</nombreEtiqueta>` se denomina cierre de la etiqueta. Entre ambas etiquetas es donde iría el contenido.

## Estructura

```HTML
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Document</title>
  </head>
  <body>

  </body>
</html>
```

`<!DOCTYPE html>` define el estándar de documento que se va a seguir, esta en concreto representa que se está siguiendo el estándar HTML5.

`<html lang="es">` establece que el documento está en español.

`<head>` representa una colección de metadatos del documento. Son datos que se le pasan al navegador para que interprete la página web. No tiene una representación visual, solo es información para el navegador.

`<title>Document</title>` representa el título de la web. Es el texto que aparece en la pestaña del navegador.

`<body>` representa todo el contenido de nuestra web. Esta parte si es visible y será visible desde el navegador.

## Comentarios

```HTML
<!-- Comentario -->
```

El texto solo es informativo para saber que está pasando en el documento o que significa lo que hay. No se mostará en la página web.

[Volver al índice](../README.md)
