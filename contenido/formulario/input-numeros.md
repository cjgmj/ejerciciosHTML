# Inputs de números

## Number

```HTML
<input type="number">
```

Se utiliza para introducir valores numéricos.

## Tel

```HTML
<input type="tel">
```

Se utiliza para introducir números telefónicos.

[Volver a Tipos de input](./tipos-input.md)
