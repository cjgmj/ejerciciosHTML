# Atributos generales

Son valores adicionales que configuran los elementos y/o ajustan su comportamiento.

En términos generales hay dos tipos de atributos:

- Comunes. Su sintaxis es `atributo="valor"`.
- Booleanos. Su sintaxis es `atributo`. Esto sucede porque en HTML hay ciertos atributos que si existen se dan como verdaderos y si no existen se dan como falso, por lo que no necesitan que se le pongan un valor.

## Class

```HTML
class=""
```

Se usa para dar estilos a través de CSS.

## Id

```HTML
id=""
```

Es un identificador único que se utiliza para seleccionar el elemento desde JS y para hacer navegación a través de anclas. También puede ser utilizado para dar estilos a través de CSS.

## Title

```HTML
title=""
```

Sirve para mostrar información de lo que está representando. Ayuda a la accesibilidad mostrando una descripción del elemento al que pertenece en un tooltip.

## Data

```HTML
data-*=""
```

Permite guardar un valor en la etiqueta HTML. La palabra detrás del guión será el identificador para obtener el valor asignado a través de JavaScript.

[Volver al índice](../README.md)
