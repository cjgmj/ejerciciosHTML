# Inputs de texto

## Email

```HTML
<input type="email">
```

Se utiliza para introducir un email.

## Password

```HTML
<input type="password">
```

Se utiliza para introducir contraseñas.

## Search

```HTML
<input type="search">
```

Se utiliza para introducir una barra de búsqueda.

## Text

```HTML
<input type="text">
```

Se utiliza para introducir textos. Es el valor por defecto de las etiquetas `input`.

## Url

```HTML
<input type="url">
```

Se utiliza para introducir URLs.

[Volver a Tipos de input](./tipos-input.md)
